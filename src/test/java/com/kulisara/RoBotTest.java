package com.kulisara;

import static org.junit.Assert.assertEquals;

import java.rmi.AccessException;

import org.junit.Test;

public class RoBotTest {
    @Test
    public void shouldCreateRoBotSuccess() {
        RoBot robot = new RoBot("robot", 'R', 10, 11);
        assertEquals("robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldRobotSuccess1() {
        RoBot robot = new RoBot("robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }
    @Test
    public void shouldRobotSuccess2() {
        RoBot robot = new RoBot("robot", 'R');
        assertEquals("robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotSuccess3() {
        RoBot robot = new RoBot("robot", 'R',10,11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldRobotfail1() {
        RoBot robot = new RoBot("robot", 'R',10,11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldRobotUpFailatmin() {
        RoBot robot = new RoBot("robot", 'R', 10, RoBot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(RoBot.Y_MIN, robot.getY());
    }

    @Test
    public void shouldRobotUpSuccess() {
        RoBot robot = new RoBot("robot", 'R', 10,11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldRobotDownSuccess() {
        RoBot robot = new RoBot("robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotDownSuccess2() {
        RoBot robot = new RoBot("robot", 'R', 19, 9);
        boolean result = robot.down(11);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRobotDownAtMax() {
        RoBot robot = new RoBot("robot", 'R', 10, RoBot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(RoBot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldRight() {
        RoBot robot = new RoBot("robot", 'R', RoBot.X_MAX, 0);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(RoBot.X_MAX, robot.getX());
    }

    @Test
    public void shouldRightSuccess() {
        RoBot robot = new RoBot("robot", 'R', 1, 0);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(2, robot.getX());
    }


    @Test
    public void shouldleft() {
        RoBot robot = new RoBot("robot", 'R', RoBot.X_MIN, 0);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(robot.X_MIN, robot.getX());
    }

    @Test
    public void shouldleftSuccess() {
        RoBot robot = new RoBot("robot", 'R', 1, 0);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }

}
