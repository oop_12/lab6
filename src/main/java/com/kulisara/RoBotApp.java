package com.kulisara;

public class RoBotApp {
    public static void main(String[] args) {
        RoBot body = new RoBot("Body",'B',1,0);
        RoBot petter = new RoBot("perrer",'P',10,10);
        body.right();
        body.print();
        petter.print();
        for(int y=RoBot.Y_MIN;y<=RoBot.Y_MAX;y++){
            for(int x=RoBot.X_MIN;x<=RoBot.X_MAX;x++){
                if(body.getX()==x &&body.getY()==y){
                    System.out.print(body.getSymbol());
                }else if(petter.getX()==x && petter.getY()==y){
                    System.out.print(petter.getSymbol());
                }else{
                    System.out.print("-");
                }
                
            }
            System.out.println();
        }

    }
    
}
