package com.kulisara;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank Kulisara = new BookBank("Kulisara",100.0);
        Kulisara.deposit(50);
        Kulisara.print();

        BookBank prayood = new BookBank("prayood",1.0);
        prayood.deposit(10000);
        prayood.withdraw(100000);
        prayood.print();

        BookBank praweet = new BookBank("praweet",10.0);
        praweet.deposit(20000);
        praweet.withdraw(10000);
        praweet.print();
    }
}
